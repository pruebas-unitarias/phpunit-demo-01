# Experimentos con PHPUnit 8

## Creación

Originalmente, así fue creado el proyecto

```
$ composer init --name danielbg/phpunit-demo-01 

$ composer require phpunit/phpunit ^8

```

## Instalación

Pero para instalarlo basta clonarlo a un directorio local y correr composer.
```
$ composer install
```

## Requerimientos técnicos
1. PHP. Se probó con PHP 7.4.5 en junio de 2020
2. Composer.  Se probó con Composer 1.9.3
3. PHPUnit. Se probó con PHPUnit 8.0

