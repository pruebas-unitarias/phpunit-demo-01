<?php

/**
 * Mailer V3 Probando dependencias estáticas
 *
 * An example mailer class
 */
class MailerV3
{

    /**
     * Send a message
     *
     * @param string $email  Recipient email address
     * @param string $message  Content of the message
     *
     * @throws InvalidArgumentException If $email is empty
     *
     * @return boolean
     */
    public function send(string $email, string $message)
    {
        if (empty($email)) {
            throw new InvalidArgumentException;
        }

        echo "Send '$message' to $email";

        return true;
    }
}
