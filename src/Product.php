<?php

/**
 * Product
 *
 * An example product class
 */
class Product
{
    /**
     * Unique identifier
     * @var integer
     */
    protected $id;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->id = rand();
    }
}
