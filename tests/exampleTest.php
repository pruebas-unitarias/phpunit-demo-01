<?php

require("vendor".DIRECTORY_SEPARATOR."autoload.php");

use PHPUnit\Framework\TestCase;

/*
Desde la raiz del proyecto, ejecutar con:
.\vendor\bin\phpunit tests\exampleTest.php
*/

class ExampleTest extends TestCase {

    public function testSumaDosMasDos() {
        require("functions.php");
        $this->assertEquals(4, add(2, 2));
    }

    public function testAddDoesNotReturnTheIncorrectSum(){
        $this->assertNotEquals(5, add(2,2));
    }

}
