<?php

use PHPUnit\Framework\TestCase;

class MailerV2Test extends TestCase {

    public function testSendMessageReturnsTrue(){
        $this->assertTrue(MailerV2::send('daniel@example.net', 'Hola'));
    }

    public function testInvalidArgumentsExceptionIfEmailIsEmpty(){
        $this->expectException(InvalidArgumentException::class);
        MailerV2::send('', '');
    }
}