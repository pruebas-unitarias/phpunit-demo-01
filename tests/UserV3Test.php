<?php

use PHPUnit\Framework\TestCase;

class UserV3Test extends TestCase {


    public function tearDown(): void {
        Mockery::close();
    }

    public function testNotifyReturnsTrue() {
        $user = new UserV3('daniel@example.com');

        $mock = Mockery::mock('alias:MailerV3');
        $mock->shouldReceive('send')
             ->once()
             ->with($user->email, 'Hello!')
             ->andReturn(true);

        $user->setMailer($mock);

        $this->assertTrue($user->notify('Hello!'));
    }
}