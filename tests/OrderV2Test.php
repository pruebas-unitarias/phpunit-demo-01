<?php

use PHPUnit\Framework\TestCase;

class OrderV2Test extends TestCase {

    public function tearDown() : void {
        Mockery::close();
    }

    public function testOrderIsProcessedUsingAMock(){
        $order = new OrderV2(3, 1.99);
        $this->assertEquals(5.97, $order->amount);

        $gateway = Mockery::mock('PaymentGateway');
        $gateway->shouldReceive('charge')
                ->once()
                ->with(5.97);

        $order->process($gateway);
    }

    public function testOrderIsProcessedUsingSpy() {
        $order = new OrderV2(3, 1.99);
        $this->assertEquals(5.97, $order->amount);

        $gateway_spy = Mockery::spy('PaymentGateway');
        $order->process($gateway_spy);

        $gateway_spy->shouldHaveReceived('charge')
                    ->once()
                    ->with(5.97);
    }

}