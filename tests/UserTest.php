<?php

use PHPUnit\Framework\TestCase;

class TestUser extends TestCase {

    public function testReturnsFullName() {
        $user = new User();
        $user->first_name = "Juan";
        $user->surname = "Perez";
        $this->assertEquals('Juan Perez', $user->getFullName());
    }

    public function testFullNameIsEmptyByDefault(){
        $user = new User;
        $this->assertEmpty($user->getFullName());
    }

    public function testNotificationIsSent() {
        $user = new User();
        $user->email = 'daniel@example.net';

        $mock_mailer = $this->createMock(Mailer::class);
        $mock_mailer->expects($this->once())
                    ->method('sendMessage')
                    ->with($this->equalTo('daniel@example.net'), $this->equalTo('Hello'))
                    ->willReturn(true);

        $user->setMailer($mock_mailer);
        $this->assertTrue($user->notify('Hello'));
    }

    public function testCannotNotifyWithoutEmail(){
        $user = new User();
        $mock_mailer = $this->getMockBuilder(Mailer::class)
                            ->setMethods(null)
                            ->getMock();

        $user->setMailer($mock_mailer);
        $this->expectException(Exception::class);
        $user->notify('Hello');
    }
}