<?php

use PHPUnit\Framework\TestCase;

class AbstractPersonTest extends TestCase {

    public function testNameAndTitleIncludeGetTitleValue() {
        $mock = $this->getMockBuilder(AbstractPerson::class)
                     ->setConstructorArgs(['Green'])
                     ->getMockForAbstractClass();

        $mock->method('getTitle')
             ->willReturn('Dr.');

        $this->assertEquals('Dr. Green', $mock->getNameAndTitle());
    }
}